from ObjetoSeguro import ObjetoSeguro
import logging
def main():
    try:
        logging.basicConfig(filename='terminar.log',
                        filemode='a', format='%(asctime)s : %(levelname)s : %(message)s',
                        datefmt='%d/%m/%y %H:%M:%S',
                        level=logging.DEBUG
                        )
        logging.debug("Creando a alice")
        alice = ObjetoSeguro()
        alice_key = alice.public_key

        logging.debug('Creando a bob')
        bob = ObjetoSeguro()
        bob_key = bob.public_key

        logging.debug("Cifrando el mensaje")
        message = "Hola!!!"
        print("Plaintext: ", message)
        logging.debug("Plaintext:" + message)
        msg_ciffer = alice.cypher(message, bob_key)
        print(msg_ciffer)
        logging.debug(msg_ciffer)
        logging.debug("Descifrando el mensaje")
        message_decipher = ObjetoSeguro.decipher(bob, msg_ciffer)
        print(message_decipher)
        logging.debug(message_decipher)

    except :
        print('Error inesperado')
        logging.fatal('Error inesperado')

if __name__ == "__main__":
    main()
